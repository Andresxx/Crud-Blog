package com.ucbcba.demo.repository;

import com.ucbcba.demo.entities.Coment;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface CommentRepository extends CrudRepository<Coment, Integer> {
}
