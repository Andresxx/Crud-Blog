package com.ucbcba.demo.controllers;

import com.ucbcba.demo.entities.Post;
import com.ucbcba.demo.entities.PostCategory;
import com.ucbcba.demo.entities.Usuario;
import com.ucbcba.demo.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PostController {
    private PostService postService;
    private PostCategoryService postCategoryService;
    private UsuarioService usuarioService;


//    @Autowired
//    public UsuarioService getUsuarioService() {
//        return usuarioService;
//    }

    @Autowired
    public void setUsuarioService(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @Autowired
    public void setPostService(PostService productService) {
        this.postService = productService;
    }

    @Autowired
    public void setPostCategoryService(PostCategoryService postCategoryService) {
        this.postCategoryService = postCategoryService;
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public String list(Model model) {
        Iterable<Usuario> usuarios = usuarioService.listAllUsuario();
        model.addAttribute("usuarios",usuarios);

        Iterable<Post> postList = postService.listAllPosts();
        model.addAttribute("variableTexto","Hello world");
        model.addAttribute("postList",postList);
        return "posts";
    }

    @RequestMapping(value = "/",method = RequestMethod.GET)
    String raiz(Model model){
        model.addAttribute("posts",postService.listAllPosts());
        return "/";
    }

    @RequestMapping("/newPost")
    String newPost(Model model) {


        Iterable<Usuario> usuarios = usuarioService.listAllUsuario();
        model.addAttribute("usuarios",usuarios);

        Iterable<PostCategory> postCategories = postCategoryService.listAllPostCategorys();
        model.addAttribute("postCategories", postCategories);

        return "newPost";
    }

    @RequestMapping(value = "/post", method = RequestMethod.POST)
    String save(Post post) {
        if(post.getText().length() <= 100)
        {

            java.util.Date d= new  java.util.Date();
            java.sql.Date d2= new  java.sql.Date(d.getTime());
            post.setFecha(d2);
            postService.savePost(post);
        }

        return "redirect:/posts";
    }

    @RequestMapping("/post/{id}")
    String show(@PathVariable Integer id, Model model) {
        Post post = postService.getPost(id);
        model.addAttribute("post", post);
        return "show";
    }

    @RequestMapping("/editPost/{id}")
    String editPost(@PathVariable Integer id, Model model) {
        Post post = postService.getPost(id);
        model.addAttribute("post", post);
        Iterable<PostCategory> postCategories = postCategoryService.listAllPostCategorys();
        model.addAttribute("postCategories", postCategories);
        Iterable<Usuario> usuarios = usuarioService.listAllUsuario();
        model.addAttribute("usuarios",usuarios);
        return "editPost";
    }

    @RequestMapping("/deletePost/{id}")
    String delete(@PathVariable Integer id) {
        postService.deletePost(id);
        return "redirect:/posts";
    }

    @RequestMapping("/like/{id}")
    String like(@PathVariable Integer id) {
        Post post = postService.getPost(id);
        post.setLikes(post.getLikes()+1);
        postService.savePost(post);
        return "redirect:/post/"+post.getId();
    }

    @RequestMapping("/dislike/{id}")
    String dislike(@PathVariable Integer id) {
        Post post = postService.getPost(id);
        if (post.getLikes()!= 0){
            post.setLikes(post.getLikes()-1);
        }
        postService.savePost(post);
        return "redirect:/post/"+post.getId();
    }
}
