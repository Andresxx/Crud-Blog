package com.ucbcba.demo.controllers;

import com.ucbcba.demo.entities.Coment;
import com.ucbcba.demo.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CommentController {

    private CommentService commentService;

    @Autowired
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    @RequestMapping(value = "/coment", method = RequestMethod.POST)
    String save(Coment coment) {

        if(coment.getText().length() >0 || coment.getText().length() <=50 )
        {
            java.util.Date d= new  java.util.Date();
            java.sql.Date d2= new  java.sql.Date(d.getTime());
            coment.setFecha(d2);
            commentService.saveComment(coment);
        }
        else
        {
            commentService.deleteComment(coment.getId());
        }

        return "redirect:/post/"+ coment.getPost().getId();
    }

    @RequestMapping("/commentlike/{id}")
    String like(@PathVariable Integer id) {
        Coment coment = commentService.getComment(id);
        coment.setLikes(coment.getLikes()+1);
        commentService.saveComment(coment);
        return "redirect:/post/"+coment.getPost().getId();
        //return "redirect:/post/"+post.getId();
    }

    @RequestMapping("/commentdislike/{id}")
    String dislike(@PathVariable Integer id) {
        Coment coment = commentService.getComment(id);
        if(coment.getLikes()!=0){
            coment.setLikes(coment.getLikes()-1);
        }
        commentService.saveComment(coment);
        return "redirect:/post/"+coment.getPost().getId();

    }
}
