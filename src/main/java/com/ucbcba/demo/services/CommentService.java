package com.ucbcba.demo.services;

import com.ucbcba.demo.entities.Coment;

public interface CommentService {
    Iterable<Coment> listAllComments();

    void saveComment(Coment coment);

    Coment getComment(Integer id);

    void deleteComment(Integer id);
}
