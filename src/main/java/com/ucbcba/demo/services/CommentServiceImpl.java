package com.ucbcba.demo.services;

import com.ucbcba.demo.entities.Coment;
import com.ucbcba.demo.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service

public class CommentServiceImpl implements CommentService {

    private CommentRepository commentRepository;

    @Autowired
    @Qualifier(value = "commentRepository")
    public void setCommentRepository(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }


    @Override
    public Iterable<Coment> listAllComments() {
        return commentRepository.findAll();
    }

    @Override
    public void saveComment(Coment coment) {
        commentRepository.save(coment);
    }

    @Override
    public Coment getComment(Integer id) {
        return commentRepository.findById(id).get();
    }

    @Override
    public void deleteComment(Integer id) {
        commentRepository.deleteById(id);;
    }
}
