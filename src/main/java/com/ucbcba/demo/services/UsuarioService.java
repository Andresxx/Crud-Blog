package com.ucbcba.demo.services;

import com.ucbcba.demo.entities.Usuario;

public interface UsuarioService {

    Iterable<Usuario> listAllUsuario();

    void saveUsuario(Usuario usuario);

    Usuario getUsuario(Integer id);

    void deleteUsuario(Integer id);
}
