delete from post_category;
delete from post;
delete from  usuario;
delete from  coment;

INSERT INTO post_category(id, name) VALUES (1000, 'Deportes');
INSERT INTO post_category(id, name) VALUES (1001, 'Noticias');

INSERT INTO post(id, text, post_category_id, usuario_id,titulo,fecha) VALUES (2000, 'Post 1', 1000,50,'Titulo1','2005-02-07');
INSERT INTO post(id, text, post_category_id, usuario_id,titulo,fecha) VALUES (2002, 'Post 2', 1001,51,'Titulo2','2005-02-07');

INSERT INTO usuario(id, name) VALUES (50, 'Andres');
INSERT INTO usuario(id, name) VALUES (51, 'Diego');

INSERT INTO coment (id, text, post_id,fecha) VALUES (100, 'Bien',2000,'2005-02-07');
INSERT INTO coment (id, text, post_id,fecha) VALUES (101, 'Muy Bien',2000,'2007-02-07');
INSERT INTO coment (id, text, post_id,fecha) VALUES (102, 'Bien',2002,'2007-02-07');
INSERT INTO coment (id, text, post_id,fecha) VALUES (103, 'Muy Bien',2002,'2007-02-07');



